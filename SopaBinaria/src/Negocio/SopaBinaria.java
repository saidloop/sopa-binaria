/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Bit;
import Util.LeerMatriz_Excel;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 *
 * @author madar
 */
public class SopaBinaria {

    private Bit mySopaBinaria[][];
    public Boolean sopaFalsa[][];

    public SopaBinaria() {
    }

    /**
     * Constructor que carga la sopa a partir de un excel
     *
     * @param rutaArchivoExcel un string con la ruta y el nombre del archivo
     * @throws Exception
     */
    public SopaBinaria(String rutaArchivoExcel) throws Exception {
        LeerMatriz_Excel myExcel = new LeerMatriz_Excel(rutaArchivoExcel, 0);
        String datos[][] = myExcel.getMatriz();

        crearBytes(datos);

    }

    private void crearBytes(String datos[][]) throws Exception {

        int f = datos.length, c = datos[0].length;

        this.mySopaBinaria = new Bit[f][c];
        this.sopaFalsa = new Boolean[f][c];
        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                Bit in = null;
                this.sopaFalsa[i][j] = false;
                if (datos[i][j] == null || !"0".equals(datos[i][j]) && !"1".equals(datos[i][j])) {
                    throw new Exception("Error la matriz posee un caracter diferente de 0 y 1");
                }
                if (datos[i][j].equals("0")) {
                    in = new Bit(false);
                }
                if (datos[i][j].equals("1")) {
                    in = new Bit(true);
                }
                this.mySopaBinaria[i][j] = in;
            }
        }
    }

    @Override
    public String toString() {
        int f = this.mySopaBinaria.length;
        int c = this.mySopaBinaria[0].length;
        String msg = "";
        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                msg += this.mySopaBinaria[i][j].toString() + " ";
            }
            msg += '\n';
        }
        return msg;
    }

    public Bit[][] getMySopaBinaria() {
        return mySopaBinaria;
    }

    public Boolean[][] getMySopaFalsa() {
        return sopaFalsa;
    }

    /**
     *
     * @param decimal
     * @return
     */
    public String binario1(int decimal) {
        String binario = "";
        while (decimal > 0) {
            binario = decimal % 2 == 0 ? "0" + binario : "1" + binario;
            decimal = decimal / 2;
        }
        return binario;
    }

    /**
     * recibe como parametro un numero decimal el cual pasa a binario y lo
     * compierte en un arreglo de String
     *
     * @param decimal
     * @return un arreglo de String con el numero en binario
     */
    public String[] convertirBinario(int decimal) {
        String binario = binario1(decimal);
        String[] binarios = new String[binario.length()];
        for (int i = binario.length() - 1; decimal > 0; i--) {
            binarios[i] = String.valueOf(decimal % 2);
            decimal = decimal / 2;
        }
        return binarios;
    }

    /**
     * recibe como parametro un arreglo de string el cual convierte a un arreglo
     * de bit
     *
     * @param binarios
     * @return un arreglo de Bit con el numero en binario
     */
    public Bit[] crearBitBinario(String binarios[]) {
        Bit[] binariosBit = new Bit[binarios.length];
        for (int j = 0; j < binarios.length; j++) {
            Bit in = null;

            if (binarios[j].equals("0")) {
                in = new Bit(false);
            } else {
                in = new Bit(true);
            }
            binariosBit[j] = in;
        }
        return binariosBit;

    }

    /**
     * Se crea para contar binarios de un digito
     *
     * @param decimal
     * @return un entero con la cantidad de veces que esta en la sopa
     */
    public int getCuantasVeces0(int decimal) {
        String[] x = new String[1];
        if (decimal == 0) {
            x[0] = "0";
        }
        if (decimal == 1) {
            x[0] = "1";
        }
        Bit[] buscado = crearBitBinario(x);
        int f = this.mySopaBinaria.length;
        int c = this.mySopaBinaria[0].length;
        int contador = 0;
        for (int i = 0; i < f; i++) {
            for (int j = 0; j < c; j++) {
                if (buscado[0].isValor() == (mySopaBinaria[i][j]).isValor()) {
                    this.sopaFalsa[i][j] = true;
                    contador++;
                }

            }

        }
        return contador;
    }

    /**
     * recibe el numero que se va a buscar y cuenta cuantas veces esta en la
     * sopa de izquierda a derecha horizontalmente
     *
     * @param decimal
     * @return la cantidad de veces que este el binario de manera horizontal
     * (izquierda a derecha)
     */
    public int getCuantasVeces_Horizontal(int decimal) {
        if (decimal == 0 || decimal == 1) {
            return getCuantasVeces0(decimal);
        }
        String[] x = convertirBinario(decimal);
        Bit[] buscado = crearBitBinario(x);
        int f = this.mySopaBinaria.length;
        int c = this.mySopaBinaria[0].length;
        int contador = 0;
        for (int i = 0; i < f; i++) {
            int contadorAux = 0;
            for (int j = 0; j < c; j++) {
                if (mySopaBinaria[i][j].isValor() && c - j >= buscado.length) {
                    for (int k = j, h = 0; k < buscado.length + j - 1; k++, h++) {
                        if (buscado[h].isValor() == (mySopaBinaria[i][k]).isValor()) {
                            contadorAux++;
                        }
                        if (contadorAux == buscado.length) {
                            contador++;

                        }

                    }
                }
            }
        }

        return contador;
    }

    /**
     * recibe el numero que se va a buscar y cuenta cuantas veces esta en la
     * sopa de arriba hacia abajo verticalmente
     *
     * @param decimal
     * @return la cantidad de veces que este el binario de manera vertical
     * (arriba hacia abajo)
     */
    public int getCuantasVeces_Vertical(int decimal) {
        if (decimal == 0 || decimal == 1) {
            return getCuantasVeces0(decimal);
        }
        String[] x = convertirBinario(decimal);
        Bit[] buscado = crearBitBinario(x);
        int f = this.mySopaBinaria.length;
        int c = this.mySopaBinaria[0].length;
        int contador = 0;
        for (int j = 0, h = 0; j < c; j++) {
            for (int i = 0; i < f; i++) {
                if (buscado[h].isValor() == (mySopaBinaria[i][j]).isValor()) {

                    h++;
                }
                if (h == buscado.length) {
                    contador++;
                    h = 0;
                }
            }
            h = 0;
        }

        return contador;
    }

    /**
     * recibe el numero que se va a buscar y cuenta cuantas veces esta en la
     * sopa de izquierda a derecha diagonalmente
     *
     * @param decimal
     * @return la cantidad de veces que este el binario de manera diagonal
     * (izquierda a derecha)
     */
    public int getCuantasVeces_Diagonal_izquierda_A_Derecha(int decimal) {
        if (decimal == 0 || decimal == 1) {
            return getCuantasVeces0(decimal);
        }
        String[] x = convertirBinario(decimal);
        Bit[] buscado = crearBitBinario(x);
        int f = this.mySopaBinaria.length;
        int c = this.mySopaBinaria[0].length;
        int contador = 0;

        //primera parte diagonal superior
        for (int i = 0, h = 0; i < f; i++) {
            for (int j = 0; j <= i; j++) {
                if ((buscado[h].isValor() == (mySopaBinaria[i - j][j]).isValor())) {
                    h++;
                }
                if (h == buscado.length) {
                    contador++;
                    h = 0;
                }
            }
            h = 0;
        }
        //segunda parte diagonal inferior
        for (int i = 0, h = 0; i < f; i++) {
            for (int j = 0; j < c - i - 1; j++) {
                if (buscado[h].isValor() == (mySopaBinaria[f - j - 1][j + i + 1]).isValor()) {
                    h++;
                }
                if (h == buscado.length) {
                    contador++;
                    h = 0;
                }
            }
            h = 0;
        }
        return contador;
    }

    /**
     * recibe el numero que se va a buscar y cuenta cuantas veces esta en la
     * sopa de derecha a izquierda diagonalmente
     *
     * @param decimal
     * @return la cantidad de veces que este el binario de manera diagonal
     * (derecha a izquierda)
     */
    public int getCuantasVeces_Diagonal_Derecha_A_Izquierda(int decimal) {
        if (decimal == 0 || decimal == 1) {
            return getCuantasVeces0(decimal);
        }
        String[] x = convertirBinario(decimal);
        Bit[] buscado = crearBitBinario(x);
        Bit[] buscadoInverso = invertir(buscado);
        int f = this.mySopaBinaria.length;
        int c = this.mySopaBinaria[0].length;
        int contador = 0;

        //primera parte diagonal superior
        for (int i = 0, h = 0; i < f; i++) {
            for (int j = 0; j <= i; j++) {
                if ((buscadoInverso[h].isValor() == (mySopaBinaria[i - j][j]).isValor())) {
                    h++;
                }
                if (h == buscadoInverso.length) {
                    contador++;
                    h = 0;
                }
            }
            h = 0;
        }
        //segunda parte diagonal inferior
        for (int i = 0, h = 0; i < f; i++) {
            for (int j = 0; j < c - i - 1; j++) {
                if (buscado[h].isValor() == (mySopaBinaria[f - j - 1][j + i + 1]).isValor()) {
                    h++;
                }
                if (h == buscado.length) {
                    contador++;
                    h = 0;
                }
            }
            h = 0;
        }
        return contador;
    }

    /**
     * suma los contadores de los metodos diagonal_izquierda_a_derecha y
     * diagonal_derecha_a_izquierda con ciertas restricciones
     *
     * @param decimal
     * @return la cantidad total de veces en que binario esta en la sopa
     */
    public int getCuantasVeces_Diagonal(int decimal) {
        if (decimal == 0 || decimal == 1) {
            return getCuantasVeces0(decimal);
        }
        String[] x = convertirBinario(decimal);
        Bit[] buscado = crearBitBinario(x);
        Bit[] buscadoInverso = invertir(buscado);
        int contador = 0;
        contador = getCuantasVeces_Diagonal_izquierda_A_Derecha(decimal) + getCuantasVeces_Diagonal_Derecha_A_Izquierda(decimal);
        if (decimal == 1) {
            return contador / 2;
        }
        for (int i = 0; i < buscado.length; i++) {
            if (buscado[i].equals(buscadoInverso[i])) {
                return contador;
            }
        }
        return contador / 2;
    }

    /**
     * invertie el binario que se va a buscar
     *
     * @param buscado
     * @return un arreglo de bits con el binario a la inversa
     */
    public Bit[] invertir(Bit[] buscado) {
        Bit aux;
        for (int i = 0; i <= (buscado.length - 1) / 2; i++) {
            aux = buscado[i];
            buscado[i] = buscado[(buscado.length - 1) - i];
            buscado[(buscado.length - 1) - i] = aux;
        }
        return buscado;
    }

    /**
     * suma los contadores de busqueda con ciertas restricciones
     *
     * @param decimal
     * @return el total de veces que esta el binario en la sopa de las
     * diferentes formas posibles
     */
    public int getTotal(int decimal) {
        if (decimal == 0 || decimal == 1) {
            return getCuantasVeces0(decimal);
        }
        int total = getCuantasVeces_Diagonal(decimal) + getCuantasVeces_Horizontal(decimal) + getCuantasVeces_Vertical(decimal);
        return total;
    }

    public void crearInforme_PDF() throws Exception {
        try {
            Bit[][] sopa = mySopaBinaria;
            Boolean[][] colorear = sopaFalsa;
            //1.crear el objeto que va a formatear el pdf
            Document documento = new Document();
            //2.crear el archivo de almacenamiento
            FileOutputStream ficheroPdf = new FileOutputStream("src/Datos/SopaResuelta.pdf");
            //3.asignar la estructura al pdf archivo fisico
            PdfWriter.getInstance(documento, ficheroPdf);
            documento.open();
            Paragraph parrafo = new Paragraph();
            parrafo.setAlignment(Element.ALIGN_CENTER);
            parrafo.add("Sopa Binaria Solucionada");
            documento.add(parrafo);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            //creo la tabla
            PdfPTable tabla = new PdfPTable(sopa.length);
            //celda.setBackgroundColorHorizontal
            for (int i = 0, h = 0; i < sopa.length; i++) {
                for (int j = 0; j < sopa.length; j++) {
                    String sopita = sopa[i][j].toString();
                    PdfPCell celda = new PdfPCell(new Phrase(sopita + ""));
                    if (colorear[i][j] == true) {
                        celda.setBackgroundColor(BaseColor.RED);
                    }

                    tabla.addCell(celda);
                }

            }

            documento.add(tabla);
            documento.add(Chunk.NEWLINE);
            documento.add(Chunk.NEWLINE);
            Paragraph parrafo1 = new Paragraph();
            parrafo1.add("Desarrollado por Carlos y Said :)");
            documento.add(parrafo1);

            documento.close();
            File fichero = new File("src/Datos/SopaResuelta.pdf");
            Desktop.getDesktop().open(fichero);
        } catch (FileNotFoundException e1) {
            throw new Exception("El archivo no ha sido encontrado");
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

}
